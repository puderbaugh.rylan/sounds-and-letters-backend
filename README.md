# Sounds & Letters Back-End

### Backend for sounds and letters

## Dependencies
```
gradle
java 17
MariaDB Server
```
### Install java 17 and gradle

### Gradle
Download Release: https://gradle.org/releases/ \
Follow Instructions for your operating system: https://gradle.org/install/

### Java

### Linux
`Debian/Ubuntu` \
sudo apt update \
sudo apt install openjdk-17-jdk

`Arch` \
sudo pacman -Syu \
sudo pacman -S java17-openjdk

`OpenSUSE` \
zypper install java17-openjdk

### MariaDB
`Debian/Ubuntu` \
sudo apt install mariadb-server

`OpenSUSE` \
sudo zypper install MariaDB-server

#### MariaDB Setup
Follow instructions: https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04

To check the version type: 
```
gradle --version
java --version
mysql -V
```

### Windows
Download the installer: https://www.java.com/download/ie_manual.jsp

## Project Build

### Compile
```sh
./gradlew clean build -i
```

### Run
```sh
java -jar application.jar
```
