CREATE DATABASE soundsandletters;

USE soundsandletters;

CREATE TABLE tblQuestionnaire(
    id        VARCHAR(256),
    question  VARCHAR(256),
    answer    VARCHAR(256),
    audioPath VARCHAR(256),
    PRIMARY KEY (id)
);