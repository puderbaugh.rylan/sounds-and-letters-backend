package edu.towson.sandl.service;

import edu.towson.sandl.model.AnswerStatus;
import edu.towson.sandl.model.IDAnswer;
import edu.towson.sandl.model.QuestionAnswer;

import java.util.List;

public interface GameService {
    AnswerStatus checkAnswer(IDAnswer idAnswer);
    void saveQuestionAnswer(QuestionAnswer questionAnswer);
    QuestionAnswer getQuestionAnswer(String id);
    List<QuestionAnswer> getQuestionAnswers();
    void updateQuestionAnswer(QuestionAnswer questionAnswer);
    void deleteQuestionAnswer(String id);

}
