package edu.towson.sandl.service;

import edu.towson.sandl.controller.GameController;
import edu.towson.sandl.model.AnswerStatus;
import edu.towson.sandl.model.IDAnswer;
import edu.towson.sandl.model.QuestionAnswer;
import edu.towson.sandl.repository.QuestionnaireRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

public class GameServiceImpl implements GameService {
    private final QuestionnaireRepository questionnaireRepository;
    private static final Logger logger = LoggerFactory.getLogger(GameController.class);

    public GameServiceImpl(QuestionnaireRepository questionnaireRepository) {
        this.questionnaireRepository = questionnaireRepository;
    }

    @Override
    public void saveQuestionAnswer(QuestionAnswer questionAnswer) {
        questionnaireRepository.persist(questionAnswer.withId(UUID.randomUUID().toString()));
    }

    @Override
    public AnswerStatus checkAnswer(IDAnswer idAnswer) {
        var questionAnswer = getQuestionAnswer(idAnswer.id());

        return new AnswerStatus(
                questionAnswer.id(),
                questionAnswer.answer().equalsIgnoreCase(idAnswer.answer())
        );
    }

    @Override
    public QuestionAnswer getQuestionAnswer(String id) {
        var questionAnswer = questionnaireRepository.get(id);
        logger.info("********************Question answer: {}", questionAnswer);
        return questionnaireRepository.get(id);
    }

    @Override
    public List<QuestionAnswer> getQuestionAnswers() {
        return questionnaireRepository.getAll();
    }

    @Override
    public void updateQuestionAnswer(QuestionAnswer questionAnswer) {
        questionnaireRepository.update(questionAnswer);
    }

    @Override
    public void deleteQuestionAnswer(String id) {
        questionnaireRepository.delete(id);
    }
}
