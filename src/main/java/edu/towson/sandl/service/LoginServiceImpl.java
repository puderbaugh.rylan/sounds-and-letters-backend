package edu.towson.sandl.service;

import edu.towson.sandl.model.Account;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Scanner;

public class LoginServiceImpl implements LoginService {
    private final String accountFileNameSuffix = "account.cred";

    @Override
    public boolean createAccount(Account account) throws IOException {
        Path filePath = Paths.get("accounts", account.username(), accountFileNameSuffix);
        boolean accountCreated;

        // TODO Save to a database
        if (Files.exists(filePath)) {
            accountCreated = false;
        } else {
            Files.createDirectories(filePath.getParent());
            // TODO: Encrypt Password
            Files.writeString(
                    filePath,
                    account.username() + "," + account.password(),
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE
            );

            accountCreated = true;
        }

        return accountCreated;
    }

    @Override
    public boolean validateCredentials(Account account) throws IOException {
        boolean validCredentials = false;

        Path filePath = getFilePath(account.username());

        // TODO Retrieve credentials from a database
        if (Files.exists(filePath)) {
            try (InputStream inputStream = Files.newInputStream(filePath);
                 Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8)) {

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    List<String> fileCredentials = List.of(line.split(","));
                    String filePassword = fileCredentials.get(1);

                    validCredentials = account.password().equals(filePassword);
                }
            }
        }
        return validCredentials;
    }

    private Path getFilePath(String userName) {
        return Paths.get("accounts", userName, accountFileNameSuffix);
    }
}