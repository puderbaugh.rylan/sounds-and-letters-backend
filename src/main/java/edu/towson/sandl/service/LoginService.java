package edu.towson.sandl.service;

import edu.towson.sandl.model.Account;

import java.io.IOException;

public interface LoginService {
    boolean createAccount(Account account) throws IOException;

    boolean validateCredentials(Account account) throws IOException;
}
