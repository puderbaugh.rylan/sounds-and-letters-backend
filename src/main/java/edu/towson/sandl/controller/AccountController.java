package edu.towson.sandl.controller;

import edu.towson.sandl.model.Account;
import edu.towson.sandl.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class AccountController {
    private final LoginService loginService;

    public AccountController(@Autowired LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody Account account) throws IOException {
        // Authenticate
        if (loginService.validateCredentials(account)) {
            return ResponseEntity.ok("Login Successful!");
        }
        return ResponseEntity.ok("Invalid Credentials!");
    }

    @PostMapping("/register")
    public ResponseEntity<String> createAccount(@RequestBody Account account) throws IOException {
        //TODO Check if password matches a criteria

        // Create User
        if (loginService.createAccount(account)) {
            return ResponseEntity.ok("Account Created!");
        }
        return ResponseEntity.ok("Username Taken!");
    }
}