package edu.towson.sandl.controller;

import edu.towson.sandl.model.AnswerStatus;
import edu.towson.sandl.model.IDAnswer;
import edu.towson.sandl.model.QuestionAnswer;
import edu.towson.sandl.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/game")
public class GameController {
    private static final Logger logger = LoggerFactory.getLogger(GameController.class);
    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/question-answer")
    public void addQuestion(@RequestBody QuestionAnswer questionAnswer) {
        gameService.saveQuestionAnswer(questionAnswer);
    }

    @GetMapping("/question-answer/{id}")
    public QuestionAnswer getQuestion(@PathVariable String id) {
        return gameService.getQuestionAnswer(id);
    }

    @GetMapping("/question-answers")
    public List<QuestionAnswer> getQuestion() {
        return gameService.getQuestionAnswers();
    }

    @PutMapping("/question-answer")
    public void updateQuestionAnswer(@RequestBody QuestionAnswer questionAnswer) {
        gameService.updateQuestionAnswer(questionAnswer);
    }

    @DeleteMapping("/question-answer/{id}")
    public void deleteQuestionAnswer(@PathVariable String id) {
        gameService.deleteQuestionAnswer(id);
    }

    @PostMapping("/question-answer/check")
    public AnswerStatus checkAnswer(@RequestBody IDAnswer idAnswer) {
        logger.info("******************Checking answer for {}", idAnswer);
        return gameService.checkAnswer(idAnswer);
    }
}