package edu.towson.sandl.config;

import edu.towson.sandl.service.GameService;
import edu.towson.sandl.service.GameServiceImpl;
import edu.towson.sandl.service.LoginServiceImpl;
import edu.towson.sandl.repository.QuestionnaireRepository;
import edu.towson.sandl.repository.QuestionnaireRepositoryJdbc;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class AppConfig {
    @Bean
    public LoginServiceImpl loginService() {
        return new LoginServiceImpl();
    }

    @Bean
    public GameService gameService(QuestionnaireRepository questionnaireRepository) {
        return new GameServiceImpl(questionnaireRepository);
    }

    @Bean
    public QuestionnaireRepository questionnaireRepository(JdbcTemplate jdbcTemplate) {
        return new QuestionnaireRepositoryJdbc(jdbcTemplate);
    }

    @Bean
    public DataSource mariaDBDataSource(DatasourceProperties datasourceProperties) {
        System.out.println("Connecting to datasource with properties: " + datasourceProperties);

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(datasourceProperties.getClazz());
        dataSource.setUrl(datasourceProperties.getUrl());
        dataSource.setUsername(datasourceProperties.getUsername());
        dataSource.setPassword(datasourceProperties.getPassword());

        return dataSource;
    }


}
