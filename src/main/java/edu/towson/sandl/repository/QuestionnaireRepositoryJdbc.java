package edu.towson.sandl.repository;

import edu.towson.sandl.model.QuestionAnswer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class QuestionnaireRepositoryJdbc implements QuestionnaireRepository {
    private static final Logger logger = LoggerFactory.getLogger(QuestionnaireRepositoryJdbc.class);
    private static final String TBL_NAME = "tblQuestionnaire";
    private final JdbcTemplate jdbcTemplate;

    public QuestionnaireRepositoryJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void persist(QuestionAnswer questionAnswer) {
        String sql =
                "INSERT INTO " +
                        TBL_NAME +
                        "(id, question, answer, audioPath) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(
                sql,
                questionAnswer.id(),
                questionAnswer.question(),
                questionAnswer.answer(),
                questionAnswer.audioPath()
        );
    }


    @Override
    public List<QuestionAnswer> getAll() {
        return jdbcTemplate.query(
                "SELECT * " +
                        "FROM " +
                        TBL_NAME,
                (rs, rowNum) ->
                        new QuestionAnswer(
                                rs.getString("id"),
                                rs.getString("question"),
                                rs.getString("answer"),
                                rs.getString("audioPath")
                        )
        );
    }

    @Override
    public QuestionAnswer get(String id) {
        return jdbcTemplate.queryForObject(
                "SELECT * " +
                        "FROM " + TBL_NAME +
                        " WHERE id=?",
                (rs, rowNum) -> new QuestionAnswer(
                        rs.getString("id"),
                        rs.getString("question"),
                        rs.getString("answer"),
                        rs.getString("audioPath")
                ),
                id
        );
    }

    @Override
    public void update(QuestionAnswer questionAnswer) {
        String sql =
                "UPDATE " + TBL_NAME + " " +
                        "SET question=?, answer=?, audioPath=? " +
                        "WHERE id=?";

        jdbcTemplate.update(
                sql,
                questionAnswer.question(),
                questionAnswer.answer(),
                questionAnswer.audioPath(),
                questionAnswer.id()
        );
    }

    @Override
    public void delete(String id) {
        String sql =
                "DELETE " +
                        "FROM " + TBL_NAME + " " +
                        "WHERE id=?";
        jdbcTemplate.update(sql, id);
    }
}