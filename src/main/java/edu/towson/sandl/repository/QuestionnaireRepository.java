package edu.towson.sandl.repository;

import edu.towson.sandl.model.QuestionAnswer;

import java.util.List;

public interface QuestionnaireRepository {
    void persist(QuestionAnswer questionAnswer);
    QuestionAnswer get(String id);
    List<QuestionAnswer> getAll();
    void update(QuestionAnswer questionAnswer);
    void delete(String id);
}
