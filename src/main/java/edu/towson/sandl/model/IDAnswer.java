package edu.towson.sandl.model;

public record IDAnswer(String id, String answer) {
}
