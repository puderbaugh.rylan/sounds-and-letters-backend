package edu.towson.sandl.model;

public record QuestionAnswer(String id, String question, String answer, String audioPath) {
    public QuestionAnswer withId(String id) {
        return new QuestionAnswer(id, question, answer, audioPath);
    }
}