package edu.towson.sandl.model;

public record AnswerStatus(String id, boolean success) {
}
